import { combineReducers } from 'redux';

import loaderSet from './loader_action_reducer';
import userReducer from './user_reducer';

export const mainReducer = combineReducers({
    loaderSet, userReducer
});