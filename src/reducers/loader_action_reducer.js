import { fromJS } from 'immutable';

import {
    INCREASE_REQUEST,
    DECREASE_REQUEST,
} from '../actions/action_types';

const INITIAL_STATE = fromJS({
    requestCount: 0,
});

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case INCREASE_REQUEST: {
            const requestCount = state.get('requestCount');
            return state.set('requestCount', requestCount + 1);
        }

        case DECREASE_REQUEST: {
            const requestCount = state.get('requestCount');
            return state.set('requestCount', requestCount - 1);
        }

        default: {
            return state;
        }
    }
}