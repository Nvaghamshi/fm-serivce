import { fromJS } from 'immutable';

import {
    DASHBOARD_DETAILS, SIDEBAR
} from '../actions/user_action_types';

const INITIAL_STATE = fromJS({
    dashboardDetails: {},
    sidebarDetails: {}
});

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case DASHBOARD_DETAILS: {
            return state.set('dashboardDetails', fromJS(action.payload));
        }
        case SIDEBAR: {
            return state.set('sidebarDetails', action.payload);
        }
        default: break;
    }
    return state;
}