import { domain, axiosInstnace as axios } from "../utils/util";
import { DASHBOARD_DETAILS, SIDEBAR } from "./user_action_types";

export const getServiceNodes = data => async dispatch => {
  let response = await axios.get(`${domain}/UIDDS/v1/servicenodes`, {
    auth: data
  });
  if (response.status === 200 && response.data) {
    dispatch({
      type: DASHBOARD_DETAILS,
      payload: response.data
    });
  } else {
    console.log("error");
  }
};

export const sidebar = (data,selectedNode) => async dispatch => {
  
  // first time dashbard loading - selectedNode node is "undefined" 
  if(selectedNode != undefined) {
    // loop over the service Node and toggle "isOpen" Flag    
    function deepIterator (data) {
      if (typeof data === typeof {}) {
        for (const key in data) {
          if(data[key] === selectedNode){
            (data.isOpen = !data.isOpen)
            return data;
          }
          deepIterator(data[key]);
        }
      } 
    }
    deepIterator(data);
  }
  else 
    data.isOpen = !data.isOpen

dispatch({
    type: SIDEBAR,
    payload: data
  });
};
