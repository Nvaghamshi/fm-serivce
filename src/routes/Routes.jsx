import React, {
    Component
  } from 'react';
import { Router, Route, Redirect, Switch } from 'react-router-dom';

import AuthGuardImp from '../components/common/userInfo';
import { history } from '../history/history';
import Loader from '../components/common/Loader';
import Login from '../components/login/login';
import Dashboard from '../components/dashboard/dashboard';


export class MainRoutes extends Component {
    constructor(props) {
      super(props)
      this.state = {
        serviceNodeDepth: 1
      }
    }
    //pass to the Login Component (Children Component)
    updateState = (treeDepth)=>{
      this.setState({serviceNodeDepth: treeDepth})
    }
  
    render () {
       //Based on service Node Depth - Gernerate the level 
        let pathString = "/dashboard";    
        for(var i = 0 ; i <= this.state.serviceNodeDepth; i++) {
          pathString += "/:level" + i +"?";
        }
        
      return (
        <Router history= {history}>
        <div className= "mainClass">
            <Loader />
            <Switch>
            <Route exact path="/" component={() => <Login updateTreeDepth =  {this.updateState}/> } /> 
            <AuthGuardImp  path= {pathString} component={Dashboard} />
            </Switch>
        </div>   
    </Router>
    
      )
   
    }
  }
  export default MainRoutes;

