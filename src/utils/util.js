import axios from 'axios';

export const domain = `${process.env.DOMAIN}`;
console.log(domain, 'domain')
import { mainStore } from '../store';
import { 
  INCREASE_REQUEST,
  DECREASE_REQUEST
} from '../actions/action_types';

export const axiosInstnace= axios.create({
  baseURL: domain
});

axiosInstnace.interceptors.request.use(
  config => {
    if( mainStore ) {
        mainStore.dispatch({ type: INCREASE_REQUEST})
    }
    return config;
  },
  error=> Promise.reject(error)
)

axiosInstnace.interceptors.response.use(
  response=> {
    if(mainStore) {
        mainStore.dispatch({ type: DECREASE_REQUEST })
    }
    return response;
  },
  error=> {
    if(mainStore) {
        mainStore.dispatch({ type: DECREASE_REQUEST })
    }
  }
)

export const validateRegex = (arr) => {
  if(!arr.length) return true;
  for(let i=0; i<arr.length; i+=1) {
    if(!arr[i].value.match(arr[i].validation_regex)) return i;
  }
  return -1;
}

export const formatRequest = (data) => {
  let {arr, ub, bread, sidebarDetails, index}= data;
  if(arr.length=== index+2) return false;
  arr.splice(index+2);
  let sp= arr.join('/')
  let url= `/${sp}`;
  let displayNames= bread;
  displayNames.splice(index+1);
  displayNames= displayNames.join(',');
  return {
      0: {unique_name: ub[index+1], display_name: bread[index], side: true},
      1: sidebarDetails,
      2: url,
      3: displayNames,
      4: ub[index+1],
      5: null
  }
}

export const CLASSES = {
  pop_up_message: `Are you sure you want to logout?`,
  failure: "snakbar_hoverlay none",
  success: "snakbar_hoverlay  success_hoverlay none",
  modal_class: "popup_hoverlay noneclass",
  comm_class: 'steparrow',
  open_comm_class: 'steparrow active_steprrow',
  isOpen: 'popup_hoverlay noneclass',
  curr_modal: 'logout',
  noneclass: 'noneclass',
  popup_hoverlay: 'popup_hoverlay',
  snakbar_hoverlay: 'snakbar_hoverlay',
  steparrow: 'steparrow',
  popup_hoverlay_noneclass: 'popup_hoverlay noneclass',
  toShowPop: 'snakbar_hoverlay success_hoverlay',
  toHidePop:"snakbar_hoverlay  success_hoverlay none",
  snakbar_hoverlay_none: 'snakbar_hoverlay none',
  active_main_menu: 'active_main_menu', // blue box
  menu_n_arrow: 'active_main_menu parent_select active_arrow_submenu', //grey box with dropdown reverse
  active_menu_grey: 'active_main_menu parent_select', //grey box 
  menu_blue_end: 'active_main_menu active_arrow_submenu'
};

export const MSG = {
  param_update: 'Parameters updated successfully!',
  reset_dispenser: `Are you sure you want to reset the dispenser?`,
  abort_dispenser: `Are you sure you want to abort the currently executing command?`,
  logout: `Are you sure you want to log out?`,
  nothing_here: 'There\'s nothing in here',
  copyright: 'Copyright 2018 IDEX | All Rights Reserved.',
  creds_err: 'Invalid credentials!',
  reset_success: 'Reset Successful!',
  no_params: 'Nothing to execute!',
  abort_success: 'Abort Successful!',
  reset: 'RESET',
  abort: 'ABORT',
  aborted: 'aborted',
  completed: 'completed',
  in_progress: 'in_progress',
  settime: 2000
}

export const PATH = {
  down: "../../../../assets/images/down.png",
  logo: "../../../assets/images/logo.png",
  reset: "../../../assets/images/reset.png",
  abort: "../../../../assets/images/abort.png",
  logout: "../../../assets/images/logout.png",
  like: "../../../assets/images/like.png"
}