import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import MainRoutes from './routes/Routes';
import { mainStore } from './store';

const Main = () => (
    <Provider store={mainStore}>
        <MainRoutes />
    </Provider>
);

render(<Main />, document.getElementById('app'));
