import ReduxThunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';

import { mainReducer } from './reducers';

const createStoreMiddleware = applyMiddleware(ReduxThunk)(createStore);
export const mainStore = createStoreMiddleware(mainReducer);