import React, {
	Component
} from 'react';
import {
	connect
} from 'react-redux';
import axioss from 'axios';
import {
	domain,
	axiosInstnace as axios,
	validateRegex,
	formatRequest,
	CLASSES,
	MSG,
	PATH
} from '../../utils/util';
import {
	history
} from '../../history/history';
import ProgressModal from '../common/progressModal';
import {
	getServiceNodes,
	sidebar
} from '../../actions/user_action';
import TextField from '@material-ui/core/TextField';

export class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			parameters: [],
			currentnode: null,
			globalCommands: [],
			commands: [],
			bread: [],
			unique_name_bread: [],
			commandParams: [],
			progress: 0,
			command_step: '',
			flag: false,
			show: false,
			paramflag: false,
			wasreset: false,
			reset_status: false,
			errorOccured: [],
			currentOpen: '',
			indArr: '',
			command_name: '',
			command_state: MSG.in_progress,
			curr_modal: CLASSES.curr_modal,
			pop_up_message: CLASSES.pop_up_message,
			failure: CLASSES.failure,
			success: CLASSES.success,
			modal_class: CLASSES.modal_class,
			comm_class: CLASSES.comm_class,
			isOpen: CLASSES.isOpen,
			
		};
	}

	componentWillMount() {
		history.push('/dashboard');
		let creds = JSON.parse(sessionStorage.getItem('user_info'));
		let service = this.props.serviceNodesData;
		service = service.toJS();
		service && !service.unique_name ? this.props.getServiceNodes(creds) : '';
		this.getGlobalCommands()
	}

	shouldComponentUpdate() {
		if (this.props && this.props.sidebarDetails && this.props.sidebarDetails.unique_name) return true;
		return false;
	}

	componentWillReceiveProps(props) {
		let serviceData_I = props.serviceNodesData.toJS();
		let url = props.match.url;
		(serviceData_I && serviceData_I.unique_name && url === '/dashboard') && !this.state.paramflag ? this.getParameters(serviceData_I, serviceData_I, `/dashboard/${serviceData_I.unique_name}`, `${serviceData_I.display_name}`, `${serviceData_I.unique_name}`) : '';
	}

	closeModel = () => {
		this.setState({
			modal_class: `${CLASSES.modal_class} ${CLASSES.noneclass}`
		});
	}

	displayPopup = (data, buttonName) => {
		this.setState({
			pop_up_message: data,
			curr_modal: buttonName,
			modal_class: CLASSES.popup_hoverlay
		});
	}
	class_command = (data, ind) => {
		data === this.state.currentOpen ? this.setState({
			currentOpen: '',
			comm_class: CLASSES.steparrow
		}) : this.setState({
			currentOpen: data,
			commandParams: this.state.commands[ind].param_list
		});
	};
	getGlobalCommands = async () => {
		const response = await axios.get(`${domain}/UIDDS/v1/globalcommands`, {
			auth: JSON.parse(sessionStorage.getItem('user_info'))
		});
		if (response.status === 200 && response.data) {
			this.setState({
				globalCommands: response.data
			});
		}
	}

	bread_navigate = async index => {
		const obj = formatRequest({
			arr: this.state.unique_name_bread,
			ub: this.state.unique_name_bread,
			bread: this.state.bread,
			sidebarDetails: this.props.sidebarDetails,
			index
		});
		obj ? this.getParameters(obj[0], obj[1], obj[2], obj[3], obj[4], obj[5]) : false;
	}

	setProgressToZero = (e, done) => {
		this.setState({
			command_state: MSG.in_progress,
			progress: 0,
			reset_status: false,
			isOpen: CLASSES.popup_hoverlay_noneclass,
			command_step: '',
			wasreset: e? true : false,
			errorOccured: []
		}, ()=> {
			done('true');
		});
	}

	handleToast = (b4, after) => {
		this.setState(b4);
		let that = this;
		setTimeout(function () {
			that.setState(after)
		}, MSG.settime);
	}

	executeGlobalCommand = async cmd_name => {
		let cond= (cmd_name===MSG.abort);
		let response= await axios.post(`${domain}/UIDDS/v1/globalcommands`, JSON.stringify(cmd_name), {
			auth: JSON.parse(sessionStorage.getItem('user_info')),
			headers: {
				'Content-Type': 'application/json'
			}	
		});
		if(response && response.status===200) {
			(this.state.progress<100 && !cond) ? this.setState({reset_status: true}): this.setState({command_state: MSG.aborted})
			this.handleToast({
				success: CLASSES.toShowPop,
				message: cond? MSG.abort_success: MSG.reset_success
			}, {
				success: CLASSES.success,
				message: ""
			});
		} else if(cmd_name!==MSG.reset) {
			  this.setState({reset_status: false});
		}
	}

	runLocalCommand = async obj => {
		obj ? obj.param_list = this.state.commandParams : false;
		let isValid = validateRegex(obj.param_list);
		if (isValid !== -1 && obj.param_list.length) {
			this.handleToast({
				failure: CLASSES.snakbar_hoverlay,
				message: this.state.commandParams.length? this.state.commandParams[isValid].description: MSG.no_params,
			}, {
				failure: CLASSES.snakbar_hoverlay_none,
				message: ""
			});
			return;
		}
		let response = await axios.post(`${domain}/UIDDS/v1/servicenodes/${this.state.currentnode.unique_name}/commands`, obj, {
			auth: JSON.parse(sessionStorage.getItem('user_info')),
			headers: {
				'Content-Type': 'application/json'
			}
		});
		if(response.status==202) {
		   	this.getStateWithInterval()
			this.setState({
				isOpen: CLASSES.popup_hoverlay,
				command_name: obj.display_name
			})
		}
	}

	getState= async () => {
		let response = await axioss.get(`${domain}/UIDDS/v1/state`, {
			auth: JSON.parse(sessionStorage.getItem('user_info'))
		})
		if(response && response.status===200 && response.data) {
			let {
				progress, command_state, command_step, errors
			} = response.data;
			// case considered where progress achieved after hitting abort
			if((command_state===MSG.aborted && this.state.wasreset)){
				stop();
			} else {
				(command_state===MSG.completed || command_state===MSG.aborted || command_state==='error') && stop()
				this.setState({
					command_state: command_state,
					progress: progress,
					command_step: command_step,
					wasreset: false,
					errorOccured: errors && errors.length? errors: []
				});
			}	
		}
	}

	getStateWithInterval = () => {
		let that= this;
		let setIn = setInterval(() => {
			this.getState();
		}, MSG.settime);
		stop = () => clearInterval(setIn)
	}

	cancelParameters = async () => {
		if (!this.state.currentnode) return;
		let response = await axios.get(`${domain}/UIDDS/v1/servicenodes/${this.state.currentnode.unique_name}/parameters`, {
			auth: JSON.parse(sessionStorage.getItem('user_info'))
		});
		if (response.data && response.data.length) this.setState({
			parameters: response.data
		});
	}

	getParameters = async (node, sidebar, url, bread, unique_name) => {
		this.setState({
			paramflag: true
		});
		if (!node && sidebar && !url && !bread) {
			this.props.sidebar(sidebar,unique_name);
			this.setState({
				flag: !this.state.flag
			})
			return;
		}
		unique_name && node && !node.side ? this.props.sidebar(sidebar) : false;
		let tempArr = url.split('/');
		tempArr.splice(0, 1);
		this.setState({
			currentnode: {
				unique_name: node.unique_name,
				display_name: node.display_name
			},
			unique_name_bread: tempArr
		});
		history.push(url);
		let breadArr = bread.split(',');
		let response = await axios.get(`${domain}/UIDDS/v1/servicenodes/${node.unique_name}/parameters`, {
			auth: JSON.parse(sessionStorage.getItem('user_info'))
		});
		let respoCommands = await axios.get(`${domain}/UIDDS/v1/servicenodes/${node.unique_name}/commands`, {
			auth: JSON.parse(sessionStorage.getItem('user_info'))
		});
		if (response.data && respoCommands.data) {
			this.setState({
				parameters: response.data,
				commands: respoCommands.data,
				bread: breadArr,
				currentOpen: ''
			});
		}
	}

	handleInputChange = (index, forCommand, event) => {
		let value = forCommand ? event.target.value : event.target.value;
		let joined = forCommand ? this.state.commandParams : this.state.parameters;
		joined[index].value = value;
		forCommand ? this.setState({
			commandParams: joined
		}) : this.setState({
			parameters: joined
		});
	}

	updateParameters = async () => {
		let isValid = validateRegex(this.state.parameters);
		if (isValid !== -1) return this.handleToast({
			failure: CLASSES.snakbar_hoverlay,
			message: this.state.parameters[isValid].description
		}, {
			failure: CLASSES.snakbar_hoverlay_none,
			message: ""
		});
		let response = await axios.put(`${domain}/UIDDS/v1/servicenodes/${this.state.currentnode.unique_name}/parameters`, this.state.parameters, {
			auth: JSON.parse(sessionStorage.getItem('user_info')),
			headers: {
				'Content-Type': 'application/json'
			}
		});
		if (response.status === 200 && response.data) this.handleToast({
			success: CLASSES.toShowPop,
			message: MSG.param_update
		}, {
			success: CLASSES.toHidePop,
			message: ""
		});
	}

	logout = () => {
		sessionStorage.removeItem('user_info');
		history.push('/');
	}

	//Recursive Function - get the Children data
	renderSidebarChildren = (data) => {
		return <ul>{data.isOpen &&  data.children &&
			data.children.map((sub,index) => this.renderSidebar(sub,index, data))}</ul>;
	  };

	renderSidebar = (sub, index, data, UB = this.state.unique_name_bread) => {
		if(data != undefined) {
			sub.path = (data.path != undefined ? data.path : data.display_name) + ","  + sub.display_name
			sub.url = (data.url != undefined ? data.url : data.unique_name) + "/"  + sub.unique_name

		}
	return (<li key={sub.unique_name} className={ UB.length && UB.indexOf(sub.unique_name)!==-1 && (UB[UB.length-1]===sub.unique_name
			&& sub.isOpen? CLASSES.menu_blue_end: ( UB[UB.length-1]===sub.unique_name ? CLASSES.active_main_menu: (
			sub.isOpen ? CLASSES.menu_n_arrow: CLASSES.active_menu_grey ))) }>
				<a className={ index != undefined && !sub.children.length ? '' : 'dropdown_visible'  }>
					<span className="menu-title" onClick={this.getParameters.bind(this,sub,  this.props.sidebarDetails, `/dashboard/${sub.url}`,
					`${sub.path}`, null)}>{sub.display_name}</span>
					<span onClick={this.getParameters.bind(this,null, this.props.sidebarDetails,null,null, sub.unique_name)}> <img src={PATH.down} /> </span>
				</a>
				<div className="InnerChlid_menu_inner">
					{sub.isOpen && this.renderSidebarChildren(sub)}
				</div>	
		</li>)
		}

    render() {
        let { sidebarDetails }= this.props; 
		let data= sidebarDetails;
		let UB= this.state.unique_name_bread;
        return (
		<div className="dashboard_container">
			<div className="sider_menu">
				<div className="side_menu_header">
					<div className="side_menu_logo">
						<img src={PATH.logo} />
					</div>
					<div className="user_info_wrapper">
						<div className="user_name">{JSON.parse(sessionStorage.getItem('user_info')).username}</div>
						{/* <div className="user_email">{'afflesim123'}</div> */}
					</div>
				</div>
					<ul className="side_menu_options">
							{this.renderSidebar(data) } 
					</ul>	
			</div>
			<div className="container_right">
				<div className="right_container_inner">
					<div className="right_container_header">
						<div className="dashboard_header_left">
							{
							this.state.currentnode && this.state.currentnode.display_name?
							<div className="node_name">{this.state.currentnode.display_name}</div>:
							<div className="node_name">{data.display_name}</div>
							}
							<div className="node-breadcrumb">
								{
								this.state.bread.length ?
								this.state.bread.map((elem, index)=>{
								return(
								<span onClick={this.bread_navigate.bind(this, index)}>{elem}</span>
								)
								}):
								<snap>{data.display_name}</snap>
								}
							</div>
						</div>
						<div className="dashboard_header_right">
							{
							this.state.globalCommands &&
							this.state.globalCommands.map(elem=> {
							return (
							elem===MSG.reset? <div><button name='reset' onClick={this.displayPopup.bind(this, MSG.reset_dispenser, 'reset' )}><img
									src={PATH.reset} />{elem}</button></div>:
							<div><button onClick={this.displayPopup.bind(this,MSG.abort_dispenser, 'abort' )}><img src={PATH.abort} />{elem}</button></div>
							)
							})
							}
							<div><button name='logout' onClick={this.displayPopup.bind(this, MSG.logout, `logout`)}><img src={PATH.logout} />LOGOUT</button></div>
						</div>
					</div>
				</div>
				{
				(this.state.parameters.length || this.state.commands.length) ?
				<div className="card_container">
					<div className="card_container_inner">
						{
						this.state.parameters.length ?
						<div>
							<span className="card_header">Parameters</span>
							{
							this.state.parameters.map((obj, index)=> {
							let cls= obj.readonly? 'non_editable parameter_label': 'parameter_label'
							return ( <div className={`card_input info`}>
								<span className="tooltip-reject"><img src="../../../../assets/images/info.png" />
									<span>{obj.description}</span>
								</span>
								<TextField id="standard-dense" label={obj.display_name} className={`card_input_inner ${cls}`} margin="dense" name={obj.unique_name}
								value={this.state.parameters[index].value} onChange={this.handleInputChange.bind(this, index, false)} InputProps={{readOnly: obj.readonly}} />
							</div> )
							})
							}
							<div className="cncl_update_btns">
								<button onClick={this.cancelParameters} className="cancel_node">REVERT</button>
								<button onClick={this.updateParameters} className="update_node">UPDATE</button>
							</div>
						</div>: <div className="no_parameters">
							<span className="card_header">Parameters</span>
							<p>{MSG.nothing_here}</p>
						</div>
						}
					</div>
					<div className="card_container_inner">
						{
						this.state.commands.length ?
						<div>
							<span className="card_header">Commands</span>
							{
							this.state.commands.map((obj,index)=> {
							return (
							<div key={obj.unique_name} className="commands_container">
								<div className="step_forward">{obj.display_name}
									<span className="tooltip-command_info"><img src="../../../../assets/images/info.png" />
										<span>{obj.description}</span>
									</span>
									<span onClick={this.class_command.bind(this, obj.unique_name, index)} className={this.state.currentOpen===obj.unique_name?
									CLASSES.open_comm_class: this.state.comm_class}>
										<img src={PATH.down} /> </span>
								</div>
								{
								this.state.currentOpen=== obj.unique_name ?

								<div className="execute_head">
									{(obj.param_list && obj.param_list.length) ?
									<div>
										{obj.param_list.map((el, index)=> {
										return (
										<div key={el.unique_name} className="steps_count">
											<div className="steps_count_left">
												<span className="step_name">{el.display_name}</span>
												<TextField id="standard-dense" className={`count_value`} margin="dense" name={el.unique_name} value={this.state.commandParams[index].value}
												onChange={this.handleInputChange.bind(this, index, true)} InputProps={{readOnly: el.readonly}} />
												<span className="tooltip-execute"><img src="../../../../assets/images/info.png" />
													<span>{el.description}</span>
												</span>
											</div>
										</div>
										)
										})}
									</div>: null
									}

									<div className="execute_btn"><button onClick={this.runLocalCommand.bind(this, obj)}>EXECUTE</button></div>
								</div>: null
								}
							</div>
							)
							})
							}
						</div> : (<div className="no_parameters"><span className="card_header">Commands</span>
							<p>{MSG.nothing_here}</p>
						</div>)
						}
					</div>
				</div>: <div className="no_card_found">{MSG.nothing_here}</div>
				}
				<div className="footer_wrapper">
					{MSG.copyright}
				</div>
			</div>
			<div className={this.state.modal_class} onClick={this.closeModel}>
				<div className="logout_popup">
					<div className="remove_popup" onClick={this.closeModel}>x</div>
					<h4>{this.state.pop_up_message}</h4>
					<div className="confirm_logout">
						{
						this.state.curr_modal==='logout' &&
						<button className="yes_logout" onClick={this.logout}>YES</button>
						}
						{
						this.state.curr_modal==='reset' &&
						<button className="yes_logout" onClick={this.executeGlobalCommand.bind(this, MSG.reset)}>YES</button>
						}
						{
						this.state.curr_modal==='abort' &&
						<button className="yes_logout" onClick={this.executeGlobalCommand.bind(this, MSG.abort)}>YES</button>
						}
						<button className="no_logout" onClick={this.closeModel}>NO</button>
					</div>
				</div>
			</div>
			<div class={this.state.failure}>
				<div className={`faliure_bar `}>{this.state.message}</div>
			</div>
			<div class={this.state.success}>
				<div className={`success_bar `}>{this.state.message}</div>
			</div>
			<ProgressModal command_state={this.state.command_state} errorOccured={this.state.errorOccured} isOpen={this.state.isOpen}
			command_name={this.state.command_name} node_name={this.state.currentnode? this.state.currentnode.display_name: '' }
			progress={this.state.progress} executeGlobalCommand={this.executeGlobalCommand} setProgressToZero={this.setProgressToZero}
			reset_status={this.state.reset_status} command_step={this.state.command_step} />
		</div>
        );
    }
}

function mapStateToProps(state) {
    return {
        serviceNodesData: state.userReducer.get("dashboardDetails"),
        sidebarDetails: state.userReducer.get('sidebarDetails')
    };
}
export default connect(mapStateToProps, {
    getServiceNodes,
    sidebar
})(Dashboard);