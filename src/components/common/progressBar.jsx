import React, {
  Component
} from 'react';
import Progress from 'react-progressbar';

export class ProgressBar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      percent: 0
    }
  }

  render () {
    return (
      <React.Fragment>
        <div className="percent_progressbar">{`${this.props.percent}%`}</div>
        <Progress completed={this.props.percent} />
      </React.Fragment>
    )
  }
}

export default ProgressBar;