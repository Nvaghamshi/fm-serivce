import React from 'react';
import { connect } from 'react-redux';

const Loader = ({ requestCount }) => (requestCount ? (
    <div className="hoverlay ">
      <div className="loader">
        <img src="/../../../assets/images/load.gif" />
        <div className="loader_content">Please wait...</div>
      </div>
    </div>
    ) : null);

export default connect(state => ({
    requestCount: state.loaderSet.get('requestCount'),
}))(Loader);
