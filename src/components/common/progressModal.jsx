import React, {
    Component
} from 'react';
import ProgressBar from './progressBar';
import {
    MSG,
    CLASSES,
    PATH
} from '../../utils/util';

export class ProgressModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            classPop: CLASSES.modal_class,
            flag: false
        }
    }

    changePop = e => {
        this.props.setProgressToZero(e, cb=> {
            if(cb) {
                this.setState({
                    classPop: CLASSES.modal_class,
                    flag: true
                });
            }
        });
    };

    componentWillReceiveProps = nextProps => {
        let {
            isOpen
        } = nextProps;
        this.setState({
            classPop: isOpen
        });
    }

    render () {
        return (
        <div className={this.state.classPop}>
            <div class="logout_popup">
                <div class="popup-header">
                    <h4>Executing Command (<span>{this.props.command_name}</span>) in <span>{this.props.node_name}</span></h4>
                    <div class="status_update">Status:
                        {
                            this.props.command_state==='error' ? 
                                        <span className={'red_aborted'}>Error Occured</span>
                            :    
                            <React.Fragment><span className={this.props.command_state===MSG.in_progress? '' : 'noneclass' }>In Progress</span>
                            <span className={this.props.command_state===MSG.completed?'green': CLASSES.noneclass }>Complete</span>
                            <span className={this.props.command_state===MSG.aborted?'red_aborted': CLASSES.noneclass}>Aborted</span></React.Fragment>   
                        } 
                        
                    </div>
                </div>
                <div className={this.props.command_state===MSG.aborted || this.props.command_state==='error'? "progress_bar_container aborted_state": "progress_bar_container"}  id="myProgress">
                    <ProgressBar percent={this.props.progress}/>
                </div>
                <div class="progress_status">
                    {
                        this.props.command_state && this.props.errorOccured.length ?
                        this.props.errorOccured.map(elem=> {
                            return(
                                <React.Fragment>
                                    <p className={'red_aborted'} ><img className="danger_img" src='../../../../assets/images/danger.png'/>{`#${elem.error_code} ${elem.error_text}`}</p>
                                <div className={this.props.command_step ? 'stepcount': 'stepcount noneclass' }><span className="aborted_steps">{this.props.command_step}</span><span></span></div>
                                </React.Fragment>          
                            )
                        })
                        :
                        <React.Fragment>
                            <span className={this.props.command_state===MSG.in_progress? '' : CLASSES.noneclass}>{this.props.command_step}</span>
                            <span className={this.props.command_state===MSG.completed? '' : CLASSES.noneclass}><img className="likeImg" src={PATH.like} />Command has been successfully executed.</span>
                            <div className={this.props.command_state===MSG.aborted ? 'stepcount': 'stepcount noneclass' }><span className="aborted_steps">{this.props.command_step}</span><span></span></div>
                        </React.Fragment>
                    }
                    
                </div>
                <div class="popup-footer">
                    {
                    this.props.command_state===MSG.in_progress ? 
                    <div class="progress_btns">
                        <button class="reset_progressbar" onClick={this.props.executeGlobalCommand.bind(this, MSG.reset )}>RESET</button>
                        <button class="abort_progressbar" onClick={this.props.executeGlobalCommand.bind(this, MSG.abort )}><img
                                src={PATH.abort} />ABORT</button>
                    </div>:
                    <div class="close_command_popup">
                        <button class="close_progressbar" onClick={this.changePop.bind(this)}>CLOSE</button>
                    </div>
                }
            </div>
        </div>
        {this.props.reset_status  ? this.changePop('reset'): ''}
        </div>
        )
    }
}

export default ProgressModal;