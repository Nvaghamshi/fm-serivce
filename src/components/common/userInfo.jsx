import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getUserData, isAuthenticated } from '../../utils/util';
// import { getUserInfo } from '../../actions/admin_action';

class AuthGuardImp extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
  }
  
  render() {
    const { component: Template, ...rest }= this.props;    
      let { user_info }= this.props;
        if(user_info) {
        }
        let info= JSON.parse(sessionStorage.getItem('user_info'));
    return (
        
      <Route 
      {...rest}      
        render= {props=>
          info && info.username ?(
            <Template {...props}/>
          )
          :(
            <Redirect
              to= {{
                pathname: '/',
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }
}


function mapStateToProps(state) {
    return {
      //  userLoginInfo: state.admin.get('userLoginInfo'),
    };
}
export default connect(mapStateToProps, {
    // getUserInfo,
})(AuthGuardImp);