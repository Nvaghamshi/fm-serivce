import React, {
    Component
} from 'react';
import {
    domain,
    axiosInstnace as axios,
    CLASSES,
    MSG,
    PATH
} from '../../utils/util';
import {
    connect
} from 'react-redux';
import {
    history
} from '../../history/history';
import {
    login
} from '../../actions/user_action';
import TextField from '@material-ui/core/TextField';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            failure: CLASSES.failure,
            message: ''
        };
    }

    componentDidMount() {
        let creds = JSON.parse(sessionStorage.getItem('user_info'));
        if (creds && creds.username) history.push('/dashboard')
    }

    componentWillReceiveProps(props) {
        if (props.error) {
            this.showError(props.error)
        }
    }

    handleToast = (b4, after) => {
        this.setState(b4);
        let that = this;
        setTimeout(function () {
            that.setState(after)
        }, MSG.settime);
    }

    showError = data => {
        this.handleToast({
            failure: CLASSES.snakbar_hoverlay,
            message: data
        }, {
            failure: CLASSES.failure,
            message: ""
        })
    }

    handleInputChange = event => {
        const {
            name,
            value
        } = event.target;
        this.setState({
            [name]: value,
        });
    }

    handleKeyPress = event => (event.key === 'Enter') && this.login(event)

    userLogin = async data => {
        let response = await axios.get(`${domain}/UIDDS/v1/servicenodes`, {
            auth: data
        });
        if (response && response.status === 200 && response.data) {
            console.log("Start Date - " + new Date().getTime());
            let handleToUpdate = this.props.updateTreeDepth;
            //check the service node depth level (How many nested children)  
            const depth = ({ children = [] }) =>
                            children.length === 0
                                ? 0
                                : 1 + Math.max (...children.map (depth))
            // update the depth level count in parent component
            handleToUpdate(depth(response.data));
            sessionStorage.setItem('user_info', JSON.stringify(data))
            history.push('/dashboard');
        } else {
            this.showError(`${MSG.creds_err}`)
        }
    }

    login = async e => {
        e.preventDefault();
        if (this.state.username == '') {
            this.handleToast({
                failure: CLASSES.snakbar_hoverlay,
                message: 'Please enter username!'
            }, {
                failure: CLASSES.failure,
                message: ""
            })
            return;
        }

        if (this.state.password == '') {
            this.handleToast({
                failure: CLASSES.snakbar_hoverlay,
                message: 'Please enter password!'
            }, {
                failure: CLASSES.failure,
                message: ""
            })
            return;
        }

        const loginInfo = {
            username: this.state.username,
            password: this.state.password,
        };
        this.userLogin(loginInfo)

    }

    render() {
        return (
            <React.Fragment>
                <div class="main_container">
                    <div class="login_main_wrapper">
                        <div class="left_main_login">
                            <div class="main_section">
                                <div class="logo">
                                    <img src={PATH.logo} />
                                </div>
                                <div class="login-text">
                                    <div>Welcome to!</div>
                                    <div class="dispenser-txt">IDEX DISPENSER SERVICE</div>
                                    <p class="dispenser_para">Lorem Ipsum is simply dummy text of the printing & typesetting
                                        industry. Lorem Ipsum has been the industry's ever since the 1500s.</p>
                                </div>
                            </div>
                            <div class={this.state.failure}>
                                <div className={`faliure_bar `}>{this.state.message}</div>
                            </div>
                            <div class="footer_login">
                                {MSG.copyright}
                            </div>
                        </div>
                        <div class="right_main_login">
                            <div class="login_form">
                                <h4>LOGIN</h4>
                                <div class="common_input_wrapper">
                                    <div class="common_input">
                                        <TextField id="standard-dense" margin="dense" class="common_input_inner" name="username"
                                            onKeyPress={this.handleKeyPress} onChange={this.handleInputChange} label="User Name" />
                                    </div>
                                </div>
                                <div class="common_input_wrapper">
                                    <div class="common_input">
                                        <TextField margin="dense" id="standard-dense" class="common_input_inner" name="password"
                                            onKeyPress={this.handleKeyPress} onChange={this.handleInputChange} type="password"
                                            label="Password" />
                                    </div>
                                </div>
                                <div><button class="login-btn" onClick={this.login}>LOGIN</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    login,
})(Login);