let Express = require('express'); 
let bodyParser = require('body-parser');
let http = require('http')
let path= require('path');
let cors= require('cors');
let Config= require('config');
const app = new Express();
const root = path.normalize(`${__dirname}`);

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true,
    }));

    app.use(cors({
        allowedHeaders: ['Content-Type', 'token'],
        exposedHeaders: ['token'],
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
    }));

    app.use(Express.static(`${root}/dist`));
    app.get('/*', (req, res) => {
        res.sendFile(`${root}/dist/index.html`);
    });

    http.createServer(app).listen(3010, () => {
        console.log(`app is listening at port 3010`);
    });